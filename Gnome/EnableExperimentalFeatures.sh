#!/bin/sh

gsettings set org.gnome.mutter experimental-features '["scale-monitor-framebuffer", "xwayland-native-scaling", "variable-refresh-rate"]'
