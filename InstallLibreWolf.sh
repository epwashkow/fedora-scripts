#!/bin/sh

# https://librewolf.net/installation/fedora/

curl -fsSL https://repo.librewolf.net/librewolf.repo | pkexec tee /etc/yum.repos.d/librewolf.repo
sudo dnf install librewolf
