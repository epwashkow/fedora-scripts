#!/bin/bash

# https://asus-linux.org/guides/fedora-guide/

sudo dnf copr enable lukenukem/asus-linux
sudo dnf update

sudo dnf install asusctl supergfxctl
sudo dnf update --refresh
sudo systemctl enable supergfxd.service

echo "Reboot the system"
