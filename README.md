# Fedora Scripts

Installation scripts for Fedora Linux

## Main Scripts

1. Install all updates
2. ASUS Laptops
    1. `ASUS/InstallCore.sh` - Installs core ASUS services
    2. `ASUS/InstallRogGui.sh` - (Optional) Installs ROG GUI for Linux

## Other Scripts

* `Gnome/EnableExperimentalFeatures.sh` - Enables Gnome experimental features (if they are not already turned on), including:
    * Fractional scaling
    * Variable Refresh Rate
* `InstallLibreWolf.sh` - Installs the LibreWolf web browser
* `InstallRpmFusion.sh` - Configures the RPM Fusion repository
* `InstallVsCode.sh` - Installs Visual Studio Code
